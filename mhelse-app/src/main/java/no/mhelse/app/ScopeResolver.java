package no.mhelse.app;

import ca.uhn.fhir.rest.method.RequestDetails;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author Marte Tårnes (Capgemini)
 */
public class ScopeResolver {

    private Properties scopeProperties;

    public boolean resolveScope(List<String> scopeKeys, RequestDetails requestDetails) {

        String requestedOperation = requestDetails.getRestOperationType().name();

        try {
            scopeProperties = PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties");
        } catch(IOException e) {
            System.out.println("Properties file could not be read");
            e.printStackTrace();
        }

        String requestedResource = requestDetails.getResourceName();
        ArrayList<String> scopeList = new ArrayList<>();
        String allowedScope;
        String allowedOperation;
        String allowedResource;

        for(String key : scopeKeys) {
            String scopeString = scopeProperties.getProperty(key);
            if(scopeString != null) {
                scopeList.addAll(Arrays.asList(scopeString.split(";")));
            }
        }
        for(String scope : scopeList) {
            allowedScope = scope.split("/")[1];
            allowedOperation = allowedScope.split("\\.")[1];
            allowedResource = allowedScope.split("\\.")[0];
            if(allowedResource.equals("*") || allowedResource.equalsIgnoreCase(requestedResource)) {
                if (allowedOperation.equals("*")) {
                    return true;
                } else if ((requestedOperation.equalsIgnoreCase("READ") ||
                        requestedOperation.equalsIgnoreCase("VREAD") ||
                        requestedOperation.equalsIgnoreCase("SEARCH_TYPE")) &&
                        allowedOperation.equalsIgnoreCase("read")) {
                    return true;
                } else if ((requestedOperation.equalsIgnoreCase("CREATE")|| requestedOperation.equalsIgnoreCase("UPDATE")) && allowedOperation.equalsIgnoreCase("write")) {
                    return true;
                }
            }
        }
        return false;
    }

}
