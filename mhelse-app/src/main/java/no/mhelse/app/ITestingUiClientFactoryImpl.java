package no.mhelse.app;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BearerTokenAuthInterceptor;
import ca.uhn.fhir.util.ITestingUiClientFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by mstrand on 13.01.2016.
 */
public class ITestingUiClientFactoryImpl implements ITestingUiClientFactory {

    @Override
    public IGenericClient newClient(FhirContext fhirContext, HttpServletRequest httpServletRequest, String s) {
        FhirContext ctx = FhirContext.forDstu2();
        IGenericClient client = ctx.newRestfulGenericClient(s);
        String token = httpServletRequest.getHeader("Authorization");
        if (token != null) {
            BearerTokenAuthInterceptor authInterceptor = new BearerTokenAuthInterceptor(token.substring("Bearer".length()).trim());
            client.registerInterceptor(authInterceptor);
        }
        return client;
    }
}
