package no.mhelse.app.extensions;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.Extension;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.util.ElementUtil;

/**
 * Created by mstrand on 24.01.2016.
 */
/**
 * Definition class for adding extensions to the built-in
 * Patient resource type.
 */
@ResourceDef(name="Patient")
public class PatientWithId extends Patient implements MHelseId {
    @Child(name="myId")
    @Extension(url="http://example.com/dontuse#myId", definedLocally=false, isModifier=false)
    @Description(shortDefinition="The name of the patient's Id")
    private StringDt myId;

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && ElementUtil.isEmpty(myId);
    }

    @Override
    public StringDt getMyId() {
        return myId;
    }

    @Override
    public void setMyId(StringDt mHelseId) {
        if (myId == null) {
            myId = new StringDt();
        }
        myId = mHelseId;
    }
}
