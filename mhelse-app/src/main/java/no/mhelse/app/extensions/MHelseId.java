package no.mhelse.app.extensions;

import ca.uhn.fhir.model.primitive.StringDt;

/**
 * Created by mstrand on 24.01.2016.
 */
public interface MHelseId {
    StringDt getMyId();
    void setMyId(StringDt mHelseId);
}
