package no.mhelse.app.interceptors;

import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.method.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;

import no.mhelse.app.ScopeResolver;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * @author Mikael Strand (Capgemini)
 * @author Marte Tårnes (Capgemini)
 */

public class AuthorizationInterceptor extends InterceptorAdapter {
    private String encodedAccessToken;
    private JSONObject decodedAccessTokenBody;
    private String authorizationHeader;

    @Override
    public boolean incomingRequestPostProcessed(final RequestDetails theRequestDetails, final HttpServletRequest theRequest, final HttpServletResponse theResponse) throws AuthenticationException {

        authorizationHeader = theRequest.getHeader("Authorization");

        if(authorizationHeader == null || authorizationHeader.isEmpty()){
            throw new AuthenticationException("No Authorization Header");
        }

        encodedAccessToken = authorizationHeader.substring("Bearer".length()).trim();
        validateAccessToken(encodedAccessToken);
        decodedAccessTokenBody = getDecodedJSONObject(encodedAccessToken.split("\\.")[1]);


        ArrayList<String> scopes = getScopeList(decodedAccessTokenBody);
        ScopeResolver scopeResolver = new ScopeResolver();
        if (!scopeResolver.resolveScope(scopes, theRequestDetails)) {
            throw new AuthenticationException("You are not authorized to perform this operation");
        }

        return true;
    }

    @Override
    public boolean outgoingResponse(RequestDetails theRequestDetails, IBaseResource theResponseObject, HttpServletRequest theServletRequest, HttpServletResponse theServletResponse) throws AuthenticationException {
        if(RestOperationTypeEnum.READ.equals(theRequestDetails.getRestOperationType()) || RestOperationTypeEnum.VREAD.equals(theRequestDetails.getRestOperationType())) {
            String sub = decodedAccessTokenBody.getString("sub");
            String objectId = null;
            if (theResponseObject instanceof Patient) {
                objectId = ((ExtensionDt)((Patient) theResponseObject).getUndeclaredExtensions().get(0)).getValue().toString();
            }
            if (sub == null || objectId == null || !sub.equals(objectId)) return false;
        }
        return true;
    }

/**
     * Call introspection endpoint on identityserver for validation of access token. Throws AuthenticationException if
     * validation is not successful
     * @param accessToken to validate
     */
    private void validateAccessToken(String accessToken){
        String introspectionValidationUri = "https://oauth.ehelselab.com/connect/introspect";
        RestTemplate restTemplate = new RestTemplate();
        String authorizationHeader = "Basic " + getAuthorization();
        String contentTypeHeader = "application/x-www-form-urlencoded";
        String body = "token="+accessToken;
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authorizationHeader);
        headers.add("Content", contentTypeHeader);
        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        String result;
        try {
            result = restTemplate.postForObject(introspectionValidationUri, entity, String.class);
        } catch(RestClientException exception){
            throw new AuthenticationException("Unauthorized scope");
        }

        JSONObject claims = new JSONObject(result);
        if (claims.has("active") && !claims.getBoolean("active")) {
            throw new AuthenticationException("Access token invalid");
        }
    }

    private String getAuthorization(){
        String scopePassword = "mobile.client:WvTQZ7fKUzBQuxQR";
        return Base64.getEncoder().encodeToString(scopePassword.getBytes());
    }

    private JSONObject getDecodedJSONObject(String encodedString){
        byte[] decoded = Base64.getDecoder().decode(encodedString);
        return new JSONObject(new String(decoded));
    }

    private ArrayList<String> getScopeList(JSONObject accessToken) {
        ArrayList<String> scopeList;
        if(accessToken.get("scope") instanceof String){
            scopeList = new ArrayList<>();
            scopeList.add(accessToken.getString("scope"));
        } else if (accessToken.get("scope") instanceof JSONArray){
            scopeList = (ArrayList<String>) JSONArrayToList(accessToken.getJSONArray("scope"));
        } else {
            scopeList = new ArrayList<>();
        }
        return scopeList;
    }
    private List<String> JSONArrayToList(final JSONArray jsonArray)
    {
        List<String> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = 0;  i < jsonArray.length(); i++) {
                list.add(jsonArray.getString(i));
            }
        }
        return list;
    }
}