package no.mhelse.app;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.method.RequestDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Marte Tårnes (Capgemini)
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(PropertiesLoaderUtils.class)
public class ScopeResolverTest {
    private ScopeResolver resolver;
    private ArrayList<String> scopeKeys;
    private RequestDetails requestDetails;
    private Properties testScopeProperties;

    @Before
    public void setUp()throws IOException {
        resolver = new ScopeResolver();
        scopeKeys = new ArrayList<>();
        requestDetails = new RequestDetails();
        testScopeProperties = new Properties();
        testScopeProperties.load(new FileInputStream("src/test/resources/mHelseScopeRoles.properties"));
    }

    @Test
    public void readObservationWithWebTestClientIsAllowed()throws IOException{
        scopeKeys.add("web.test.client");
        requestDetails.setRestOperationType(RestOperationTypeEnum.VREAD);
        requestDetails.setResourceName("Observation");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertTrue(resolver.resolveScope(scopeKeys, requestDetails));
    }

    @Test
    public void writePatientWithMobileClientIsNotAllowed()throws IOException {
        scopeKeys.add("mobile.client");
        requestDetails.setRestOperationType(RestOperationTypeEnum.CREATE);
        requestDetails.setResourceName("Patient");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertFalse(resolver.resolveScope(scopeKeys, requestDetails));
    }

    @Test
    public void createObservationWithMobileClientIsAllowed()throws IOException {
        scopeKeys.add("mobile.client");
        requestDetails.setRestOperationType(RestOperationTypeEnum.CREATE);
        requestDetails.setResourceName("Observation");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertTrue(resolver.resolveScope(scopeKeys, requestDetails));
    }

    @Test
    public void readPatientWithMobileClientIsNotAllowed()throws IOException {
        scopeKeys.add("mobile.client.restricted");
        requestDetails.setRestOperationType(RestOperationTypeEnum.VREAD);
        requestDetails.setResourceName("Patient");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertFalse(resolver.resolveScope(scopeKeys, requestDetails));
    }

    @Test
    public void nonExistingScopeKeyIsNotAllowed()throws IOException {
        scopeKeys.add("non.existing.key");
        requestDetails.setRestOperationType(RestOperationTypeEnum.VREAD);
        requestDetails.setResourceName("Patient");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertFalse(resolver.resolveScope(scopeKeys, requestDetails));
    }

    @Test
    public void writePatientWithWebTestClientIsAllowed()throws IOException {
        scopeKeys.add("web.test.client");
        requestDetails.setRestOperationType(RestOperationTypeEnum.CREATE);
        requestDetails.setResourceName("Patient");
        PowerMockito.spy(PropertiesLoaderUtils.class);
        Mockito.when(PropertiesLoaderUtils.loadAllProperties("/mHelseScopeRoles.properties")).thenReturn(testScopeProperties);

        assertTrue(resolver.resolveScope(scopeKeys, requestDetails));
    }
}
